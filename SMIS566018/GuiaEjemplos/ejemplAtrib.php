<?php

// atributos
/* los atributos normalmente son derivados (private), esto significa que no se puede acceder
al mismo desde fuera de la clase.*/

$per1=new Persona();
$per1->inicializar('Juan');
$per1->imprimir();

$per2=new Persona();
$per2->inicializar('Ana');
$per2->imprimir();
?>