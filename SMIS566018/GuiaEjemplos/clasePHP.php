<?php


class Persona
{
 // definir las propiedades
 private $_nombre;
 private $_edad;
 // al defimnirlos como private indica que no se pude acceder
 // a las variavles directamente desde fuera de la clase (encapdulamiento)
 
 // metodo constructor
 public function _construct($nombre,$edad) // se crea cada vez que se crea unainstancia de la clase
 {
	 $this->$_nombre=$nombre;
	 $this->$_edad=$edad;
 }
 
 public function setNombre($nombre)// metodo set utilizado para asignar un valor a las vaiables
 {
	 $this->$_nombre=$nombre;
 }
 public function getNombre() // metodos get utilizados para acceder a los valores de las variables
 {
	 return $this->$_nombre;
 }
 public function setEdad($edad)
 {
	 $this->$_edad=$edad;
 }
 public function getEdad()
 {
	 return $_edad;
 }
}
 